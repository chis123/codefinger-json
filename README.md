#codefinger-json

### 程序入口

程序入口全在org.codefinger.json包中。
其中JSONUtil是程序的主要入口，这里提供了JSON序列化和反序列化的静态方法。

### 支持的数据类型

- 所有基本数据类型以及其包装类
- java.util.Map 接口下的所有非抽象的实现类（包含默认构造函数），泛型Key支持所有基本数据类型，泛型Value支持列表中的所有类型
- java.util.Collection 接口下的所有非抽象的实现类（包含默认的构造函数），泛型支持列表中所有类型
- 列表中所有类型的一维数组、多维数组、泛型的任意嵌套
- 任意枚举类型
- 不包含列表以外类型的属性的Pojo对象类型，可任意嵌套
- java.util.Date
- java.math.BigDecimal
- java.math.BigInteger
- java.lang.String
- java.lang.Object
- org.codefinger.json.JSON
- org.codefinger.json.JSONArray
- org.codefinger.json.JSONBase
- org.codefinger.json.JSONObject

### 高性能，高容错，无依赖

从阿里巴巴温少的fastjson中吸取了大量优化手段，序列化与饭序列化的性能与fastjson不相上下，但个人认为对于泛型或者数据类型之间的兼容比fastjson更胜一筹。并且对于原生Java对象的类型转换、序列化与反序列化的性能已超越了fastjson。在测试代码中有与fastjson的性能测试比对。

#附件中有一个编译好的版本，可以直接使用