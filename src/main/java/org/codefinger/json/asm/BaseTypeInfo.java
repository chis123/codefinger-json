package org.codefinger.json.asm;

public class BaseTypeInfo {

	private String	asmPackageClassName;

	private String	asmPackageClassType;

	private String	asmBaseClassName;

	public BaseTypeInfo(String asmPackageClassName, String asmBaseClassName, String asmPackageClassType) {
		this.asmPackageClassName = asmPackageClassName;
		this.asmBaseClassName = asmBaseClassName;
		this.asmPackageClassType = asmPackageClassType;
	}

	public String getAsmPackageClassType() {
		return asmPackageClassType;
	}

	public String getAsmPackageClassName() {
		return asmPackageClassName;
	}

	public String getAsmBaseClassName() {
		return asmBaseClassName;
	}

}
