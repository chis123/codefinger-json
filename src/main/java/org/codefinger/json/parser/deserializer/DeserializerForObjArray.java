package org.codefinger.json.parser.deserializer;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;
import org.codefinger.json.parser.JSONParser;
import org.codefinger.json.util.IdentityCache;
import org.codefinger.json.util.IdentityCache.ValueBuilder;
import org.codefinger.json.util.type.Generic;

public class DeserializerForObjArray implements JSONDeserializer {

	private static final IdentityCache<Type, InitializationDevice>	INIT_MAP	= new IdentityCache<Type, InitializationDevice>(new InitializationDeviceBuilder());

	private Type													type;

	public DeserializerForObjArray(Type type) {
		this.type = type;
	}

	@Override
	public Object stringValue(String value) {
		return null;
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		jsonLexer.skipCurrentString();
		return null;
	}

	@Override
	public Object intValue(String value) {
		return null;
	}

	@Override
	public Object floatValue(String value) {
		return null;
	}

	@Override
	public Object falseValue() {
		return null;
	}

	@Override
	public Object trueValue() {
		return null;
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		InitializationDevice device = INIT_MAP.get(type);
		return new ArrayDeserializerForArray(device.getDeserializer(), device.getClazz());
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	private static class InitializationDevice {

		private JSONDeserializer	deserializer;

		private Class<?>			clazz;

		public InitializationDevice(JSONDeserializer deserializer, Class<?> clazz) {
			super();
			this.deserializer = deserializer;
			this.clazz = clazz;
		}

		public JSONDeserializer getDeserializer() {
			return deserializer;
		}

		public Class<?> getClazz() {
			return clazz;
		}

	}

	@Override
	public Object value(Integer value) {
		return null;
	}

	@Override
	public Object value(Double value) {
		return null;
	}

	@Override
	public Object value(Float value) {
		return null;
	}

	@Override
	public Object value(Long value) {
		return null;
	}

	@Override
	public Object value(Short value) {
		return null;
	}

	@Override
	public Object value(Byte value) {
		return null;
	}

	@Override
	public Object value(Boolean value) {
		return null;
	}

	@Override
	public Object value(Character value) {
		return null;
	}

	@Override
	public Object value(Date value) {
		return null;
	}

	@Override
	public Object value(BigDecimal value) {
		return null;
	}

	@Override
	public Object value(BigInteger value) {
		return null;
	}

	@Override
	public Object value(JSONBase value) {
		return null;
	}

	private static class InitializationDeviceBuilder implements ValueBuilder<Type, InitializationDevice> {

		@Override
		public InitializationDevice build(Type type) {
			Generic generic = Generic.getGeneric(type);

			return new InitializationDevice(JSONParser.getDeserializer(generic.getComponentType()), generic.getRealClass());
		}

	}

}
