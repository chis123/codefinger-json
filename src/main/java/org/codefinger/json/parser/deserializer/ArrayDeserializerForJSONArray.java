package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONArray;
import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ArrayDeserializerForJSONArray implements JSONArrayDeserializer {

	private JSONArray	jsonArray	= new JSONArray();

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForJSONArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return new ObjectDeserializerForJSONObject();
	}

	@Override
	public void addValue(Object value) {
		jsonArray.add(value);
	}

	@Override
	public void addStringValue(JSONLexer jsonLexer) {
		jsonArray.add(jsonLexer.getJSONString());
	}

	@Override
	public void addIntValue(String value) {
		jsonArray.add(new BigDecimal(value));
	}

	@Override
	public void addFloatValue(String value) {
		jsonArray.add(new BigDecimal(value));
	}

	@Override
	public void addFalseValue() {
		jsonArray.add(false);
	}

	@Override
	public void addTrueValue() {
		jsonArray.add(true);
	}

	@Override
	public void addNullValue() {
		jsonArray.add(null);
	}

	@Override
	public Object getDeserializedValue() {
		return jsonArray;
	}

	@Override
	public void addValue(Integer value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(Double value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(Float value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(Long value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(Short value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(Byte value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(Boolean value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(Character value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(Date value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(BigDecimal value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(BigInteger value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(JSONBase value) {
		jsonArray.add(value);
	}

	@Override
	public void addValue(String value) {
		jsonArray.add(value);
	}

	@Override
	public Object to(int[] value) {
		JSONArray jsonArray = this.jsonArray;
		for (int item : value) {
			jsonArray.add(item);
		}
		return jsonArray;
	}

	@Override
	public Object to(double[] value) {
		JSONArray jsonArray = this.jsonArray;
		for (double item : value) {
			jsonArray.add(item);
		}
		return jsonArray;
	}

	@Override
	public Object to(float[] value) {
		JSONArray jsonArray = this.jsonArray;
		for (float item : value) {
			jsonArray.add(item);
		}
		return jsonArray;
	}

	@Override
	public Object to(long[] value) {
		JSONArray jsonArray = this.jsonArray;
		for (long item : value) {
			jsonArray.add(item);
		}
		return jsonArray;
	}

	@Override
	public Object to(short[] value) {
		JSONArray jsonArray = this.jsonArray;
		for (int item : value) {
			jsonArray.add(item);
		}
		return jsonArray;
	}

	@Override
	public Object to(byte[] value) {
		JSONArray jsonArray = this.jsonArray;
		for (byte item : value) {
			jsonArray.add(item);
		}
		return jsonArray;
	}

	@Override
	public Object to(boolean[] value) {
		JSONArray jsonArray = this.jsonArray;
		for (boolean item : value) {
			jsonArray.add(item);
		}
		return jsonArray;
	}

	@Override
	public Object to(char[] value) {
		JSONArray jsonArray = this.jsonArray;
		for (char item : value) {
			jsonArray.add(item);
		}
		return jsonArray;
	}

}
