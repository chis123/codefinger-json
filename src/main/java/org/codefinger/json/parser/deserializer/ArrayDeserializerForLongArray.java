package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ArrayDeserializerForLongArray implements JSONArrayDeserializer {

	private static final long[]	DEFAULTCAPACITY_EMPTY_ELEMENTDATA	= new long[0];

	private long[]				elementData							= DEFAULTCAPACITY_EMPTY_ELEMENTDATA;

	private int					size								= 0;

	public ArrayDeserializerForLongArray() {
		super();
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public void addValue(Object value) {
	}

	@Override
	public void addStringValue(JSONLexer jsonLexer) {
		add(Long.parseLong(jsonLexer.getOriginalString()));
	}

	@Override
	public void addIntValue(String value) {
		add(Long.parseLong(value));
	}

	@Override
	public void addFloatValue(String value) {
		add(new BigDecimal(value).longValue());
	}

	@Override
	public void addFalseValue() {
		add(0L);
	}

	@Override
	public void addTrueValue() {
		add(1L);
	}

	@Override
	public void addNullValue() {
	}

	@Override
	public Object getDeserializedValue() {
		return Arrays.copyOf(elementData, size);
	}

	private void ensureCapacityInternal(int minCapacity) {
		if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
			minCapacity = Math.max(10, minCapacity);
		}
		ensureExplicitCapacity(minCapacity);
	}

	private void ensureExplicitCapacity(int minCapacity) {
		if (minCapacity - elementData.length > 0) {
			grow(minCapacity);
		}
	}

	private void grow(int minCapacity) {
		int oldCapacity = elementData.length;
		int newCapacity = oldCapacity + (oldCapacity >> 1);
		if (newCapacity - minCapacity < 0) {
			newCapacity = minCapacity;
		}
		elementData = Arrays.copyOf(elementData, newCapacity);
	}

	private void add(long value) {
		ensureCapacityInternal(size + 1);
		elementData[size++] = value;
	}

	@Override
	public void addValue(Integer value) {
		add(value.longValue());
	}

	@Override
	public void addValue(Double value) {
		add(value.longValue());
	}

	@Override
	public void addValue(Float value) {
		add(value.longValue());
	}

	@Override
	public void addValue(Long value) {
		add(value.longValue());
	}

	@Override
	public void addValue(Short value) {
		add(value.longValue());
	}

	@Override
	public void addValue(Byte value) {
		add(value.longValue());
	}

	@Override
	public void addValue(Boolean value) {
		add(value ? 1 : 0);
	}

	@Override
	public void addValue(Character value) {
		add(value.charValue());
	}

	@Override
	public void addValue(Date value) {
		add(value.getTime());
	}

	@Override
	public void addValue(BigDecimal value) {
		add(value.longValue());
	}

	@Override
	public void addValue(BigInteger value) {
		add(value.longValue());
	}

	@Override
	public void addValue(JSONBase value) {
		add(value.longValue());
	}

	@Override
	public void addValue(String value) {
		add(Long.parseLong(value));
	}

	@Override
	public Object to(int[] value) {
		return Arrays.copyOf(value, value.length);
	}

	@Override
	public Object to(double[] value) {
		int length = value.length;
		long[] result = new long[length];
		for (int i = 0; i < length; i++) {
			result[i] = (long) value[i];
		}
		return result;
	}

	@Override
	public Object to(float[] value) {
		int length = value.length;
		long[] result = new long[length];
		for (int i = 0; i < length; i++) {
			result[i] = (long) value[i];
		}
		return result;
	}

	@Override
	public Object to(long[] value) {
		return Arrays.copyOf(value, value.length);
	}

	@Override
	public Object to(short[] value) {
		int length = value.length;
		long[] result = new long[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i];
		}
		return result;
	}

	@Override
	public Object to(byte[] value) {
		int length = value.length;
		long[] result = new long[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i];
		}
		return result;
	}

	@Override
	public Object to(boolean[] value) {
		int length = value.length;
		long[] result = new long[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i] ? 1 : 0;
		}
		return result;
	}

	@Override
	public Object to(char[] value) {
		int length = value.length;
		long[] result = new long[length];
		for (int i = 0; i < length; i++) {
			result[i] = value[i];
		}
		return result;
	}

}
