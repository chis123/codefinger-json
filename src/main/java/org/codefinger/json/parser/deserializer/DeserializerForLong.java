package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForLong implements JSONDeserializer {

	public static final DeserializerForLong	INSTANCE	= new DeserializerForLong();

	private DeserializerForLong() {

	}

	@Override
	public Object stringValue(String value) {
		return Long.valueOf(value);
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return Long.valueOf(jsonLexer.getOriginalString());
	}

	@Override
	public Object intValue(String value) {
		return Long.valueOf(value);
	}

	@Override
	public Object floatValue(String value) {
		return Long.valueOf(new BigDecimal(value).longValue());
	}

	@Override
	public Object falseValue() {
		return Long.valueOf(0);
	}

	@Override
	public Object trueValue() {
		return Long.valueOf(1);
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return value.longValue();
	}

	@Override
	public Object value(Double value) {
		return value.longValue();
	}

	@Override
	public Object value(Float value) {
		return value.longValue();
	}

	@Override
	public Object value(Long value) {
		return value;
	}

	@Override
	public Object value(Short value) {
		return value.longValue();
	}

	@Override
	public Object value(Byte value) {
		return value.longValue();
	}

	@Override
	public Object value(Boolean value) {
		return value ? 1L : 0L;
	}

	@Override
	public Object value(Character value) {
		return (long) value.charValue();
	}

	@Override
	public Object value(Date value) {
		return value.getTime();
	}

	@Override
	public Object value(BigDecimal value) {
		return value.longValue();
	}

	@Override
	public Object value(BigInteger value) {
		return value.longValue();
	}

	@Override
	public Object value(JSONBase value) {
		return value.longValue();
	}

}
