package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForChar implements JSONDeserializer {

	public static final DeserializerForChar	INSTANCE	= new DeserializerForChar();

	private DeserializerForChar() {

	}

	@Override
	public Object stringValue(String value) {
		if (value.length() > 0) {
			return Character.valueOf(value.charAt(0));
		}
		return null;
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		String value = jsonLexer.getJSONString();
		if (value.length() > 0) {
			return Character.valueOf(value.charAt(0));
		}
		return null;
	}

	@Override
	public Object intValue(String value) {
		return Character.valueOf((char) Integer.parseInt(value));
	}

	@Override
	public Object floatValue(String value) {
		return Character.valueOf((char) new BigDecimal(value).intValue());
	}

	@Override
	public Object falseValue() {
		return null;
	}

	@Override
	public Object trueValue() {
		return null;
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return (char) value.intValue();
	}

	@Override
	public Object value(Double value) {
		return (char) value.intValue();
	}

	@Override
	public Object value(Float value) {
		return (char) value.intValue();
	}

	@Override
	public Object value(Long value) {
		return (char) value.intValue();
	}

	@Override
	public Object value(Short value) {
		return (char) value.intValue();
	}

	@Override
	public Object value(Byte value) {
		return (char) value.intValue();
	}

	@Override
	public Object value(Boolean value) {
		return value ? (char) 1 : 0;
	}

	@Override
	public Object value(Character value) {
		return value;
	}

	@Override
	public Object value(Date value) {
		return (char) value.getTime();
	}

	@Override
	public Object value(BigDecimal value) {
		return (char) value.intValue();
	}

	@Override
	public Object value(BigInteger value) {
		return (char) value.intValue();
	}

	@Override
	public Object value(JSONBase value) {
		return (char) value.intValue().intValue();
	}

}
