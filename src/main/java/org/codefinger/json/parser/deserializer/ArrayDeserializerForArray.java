package org.codefinger.json.parser.deserializer;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ArrayDeserializerForArray implements JSONArrayDeserializer {

	private JSONDeserializer	deserializer;

	private Class<?>			componentType;

	private ArrayList<Object>	arrayList	= new ArrayList<Object>();

	public ArrayDeserializerForArray(JSONDeserializer deserializer, Class<?> componentType) {
		super();
		this.deserializer = deserializer;
		this.componentType = componentType;
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return deserializer.createArrayDeserializer();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return deserializer.createObjectDeserializer();
	}

	@Override
	public void addValue(Object value) {
		arrayList.add(value);
	}

	@Override
	public void addStringValue(JSONLexer jsonLexer) {
		arrayList.add(deserializer.stringValue(jsonLexer));
	}

	@Override
	public void addIntValue(String value) {
		arrayList.add(deserializer.intValue(value));
	}

	@Override
	public void addFloatValue(String value) {
		arrayList.add(deserializer.floatValue(value));
	}

	@Override
	public void addFalseValue() {
		arrayList.add(deserializer.falseValue());
	}

	@Override
	public void addTrueValue() {
		arrayList.add(deserializer.trueValue());
	}

	@Override
	public void addNullValue() {
		arrayList.add(null);
	}

	@Override
	public Object getDeserializedValue() {
		return arrayList.toArray((Object[]) Array.newInstance(componentType, arrayList.size()));
	}

	@Override
	public void addValue(Integer value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(Double value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(Float value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(Long value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(Short value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(Byte value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(Boolean value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(Character value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(Date value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(BigDecimal value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(BigInteger value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(JSONBase value) {
		arrayList.add(deserializer.value(value));
	}

	@Override
	public void addValue(String value) {
		arrayList.add(deserializer.stringValue(value));
	}

	@Override
	public Object to(int[] value) {
		for (int item : value) {
			arrayList.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(double[] value) {
		for (double item : value) {
			arrayList.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(float[] value) {
		for (float item : value) {
			arrayList.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(long[] value) {
		for (long item : value) {
			arrayList.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(short[] value) {
		for (short item : value) {
			arrayList.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(byte[] value) {
		for (byte item : value) {
			arrayList.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(boolean[] value) {
		for (boolean item : value) {
			arrayList.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(char[] value) {
		for (char item : value) {
			arrayList.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

}
