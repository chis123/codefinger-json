package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForFloatArray implements JSONDeserializer {

	public static final DeserializerForFloatArray	INSTANCE	= new DeserializerForFloatArray();

	private DeserializerForFloatArray() {

	}

	@Override
	public Object stringValue(String value) {
		return new float[] { Float.parseFloat(value) };
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return new float[] { Float.parseFloat(jsonLexer.getOriginalString()) };
	}

	@Override
	public Object intValue(String value) {
		return new float[] { Float.parseFloat(value) };
	}

	@Override
	public Object floatValue(String value) {
		return new float[] { Float.parseFloat(value) };
	}

	@Override
	public Object falseValue() {
		return new float[] { 0 };
	}

	@Override
	public Object trueValue() {
		return new float[] { 1 };
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForFloatArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return new float[] { value.floatValue() };
	}

	@Override
	public Object value(Double value) {
		return new float[] { value.floatValue() };
	}

	@Override
	public Object value(Float value) {
		return new float[] { value.floatValue() };
	}

	@Override
	public Object value(Long value) {
		return new float[] { value.floatValue() };
	}

	@Override
	public Object value(Short value) {
		return new float[] { value.floatValue() };
	}

	@Override
	public Object value(Byte value) {
		return new float[] { value.floatValue() };
	}

	@Override
	public Object value(Boolean value) {
		return new float[] { value ? 1F : 0F };
	}

	@Override
	public Object value(Character value) {
		return new float[] { value.charValue() };
	}

	@Override
	public Object value(Date value) {
		return new float[] { value.getTime() };
	}

	@Override
	public Object value(BigDecimal value) {
		return new float[] { value.floatValue() };
	}

	@Override
	public Object value(BigInteger value) {
		return new float[] { value.floatValue() };
	}

	@Override
	public Object value(JSONBase value) {
		return new float[] { value.floatValue() };
	}

}
