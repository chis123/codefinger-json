package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForShort implements JSONDeserializer {

	public static final DeserializerForShort	INSTANCE	= new DeserializerForShort();

	private DeserializerForShort() {

	}

	@Override
	public Object stringValue(String value) {
		return Short.valueOf(value);
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return Short.valueOf(jsonLexer.getOriginalString());
	}

	@Override
	public Object intValue(String value) {
		return Short.valueOf(value);
	}

	@Override
	public Object floatValue(String value) {
		return Short.valueOf(new BigDecimal(value).shortValue());
	}

	@Override
	public Object falseValue() {
		return Short.valueOf((short) 0);
	}

	@Override
	public Object trueValue() {
		return Short.valueOf((short) 1);
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return value.shortValue();
	}

	@Override
	public Object value(Double value) {
		return value.shortValue();
	}

	@Override
	public Object value(Float value) {
		return value.shortValue();
	}

	@Override
	public Object value(Long value) {
		return value.shortValue();
	}

	@Override
	public Object value(Short value) {
		return value;
	}

	@Override
	public Object value(Byte value) {
		return value.shortValue();
	}

	@Override
	public Object value(Boolean value) {
		return value ? (short) 1 : 0;
	}

	@Override
	public Object value(Character value) {
		return (short) value.charValue();
	}

	@Override
	public Object value(Date value) {
		return (short) value.getTime();
	}

	@Override
	public Object value(BigDecimal value) {
		return value.shortValue();
	}

	@Override
	public Object value(BigInteger value) {
		return value.shortValue();
	}

	@Override
	public Object value(JSONBase value) {
		return value.shortValue();
	}

}
