package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForBoolean implements JSONDeserializer {

	public static final DeserializerForBoolean	INSTANCE	= new DeserializerForBoolean();

	private DeserializerForBoolean() {

	}

	@Override
	public Object stringValue(String value) {
		return Boolean.valueOf(value);
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return Boolean.valueOf(jsonLexer.getOriginalString());
	}

	@Override
	public Object intValue(String value) {
		return Boolean.valueOf(new BigDecimal(value).intValue() != 0);
	}

	@Override
	public Object floatValue(String value) {
		return Boolean.valueOf(new BigDecimal(value).intValue() != 0);
	}

	@Override
	public Object falseValue() {
		return Boolean.FALSE;
	}

	@Override
	public Object trueValue() {
		return Boolean.TRUE;
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return value != 0;
	}

	@Override
	public Object value(Double value) {
		return value != 0;
	}

	@Override
	public Object value(Float value) {
		return value != 0;
	}

	@Override
	public Object value(Long value) {
		return value != 0;
	}

	@Override
	public Object value(Short value) {
		return value != 0;
	}

	@Override
	public Object value(Byte value) {
		return value != 0;
	}

	@Override
	public Object value(Boolean value) {
		return value;
	}

	@Override
	public Object value(Character value) {
		return value != 0;
	}

	@Override
	public Object value(Date value) {
		return value.getTime() != 0;
	}

	@Override
	public Object value(BigDecimal value) {
		return value.intValue() != 0;
	}

	@Override
	public Object value(BigInteger value) {
		return value.intValue() != 0;
	}

	@Override
	public Object value(JSONBase value) {
		return value.booleanValue();
	}

}
