package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForDouble implements JSONDeserializer {

	public static final DeserializerForDouble	INSTANCE	= new DeserializerForDouble();

	private DeserializerForDouble() {

	}

	@Override
	public Object stringValue(String value) {
		return Double.valueOf(value);
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return Double.valueOf(jsonLexer.getOriginalString());
	}

	@Override
	public Object intValue(String value) {
		return Double.valueOf(value);
	}

	@Override
	public Object floatValue(String value) {
		return Double.valueOf(value);
	}

	@Override
	public Object falseValue() {
		return Double.valueOf(0);
	}

	@Override
	public Object trueValue() {
		return Double.valueOf(1);
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return value.doubleValue();
	}

	@Override
	public Object value(Double value) {
		return value;
	}

	@Override
	public Object value(Float value) {
		return value.doubleValue();
	}

	@Override
	public Object value(Long value) {
		return value.doubleValue();
	}

	@Override
	public Object value(Short value) {
		return value.doubleValue();
	}

	@Override
	public Object value(Byte value) {
		return value.doubleValue();
	}

	@Override
	public Object value(Boolean value) {
		return value ? 1.0 : 0.0;
	}

	@Override
	public Object value(Character value) {
		return (double) value.charValue();
	}

	@Override
	public Object value(Date value) {
		return (double) value.getTime();
	}

	@Override
	public Object value(BigDecimal value) {
		return value.doubleValue();
	}

	@Override
	public Object value(BigInteger value) {
		return value.doubleValue();
	}

	@Override
	public Object value(JSONBase value) {
		return value.doubleValue();
	}

}
