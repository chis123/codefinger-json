package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForJSON implements JSONDeserializer {

	public static final DeserializerForJSON	INSTANCE	= new DeserializerForJSON();

	private DeserializerForJSON() {

	}

	@Override
	public Object stringValue(String value) {
		return new JSONBase(value);
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return new JSONBase(jsonLexer.getJSONString());
	}

	@Override
	public Object intValue(String value) {
		return new JSONBase(new BigDecimal(value));
	}

	@Override
	public Object floatValue(String value) {
		return new JSONBase(new BigDecimal(value));
	}

	@Override
	public Object falseValue() {
		return new JSONBase(false);
	}

	@Override
	public Object trueValue() {
		return new JSONBase(true);
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForJSONArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return new ObjectDeserializerForJSONObject();
	}

	@Override
	public Object value(Integer value) {
		return new JSONBase(value);
	}

	@Override
	public Object value(Double value) {
		return new JSONBase(value);
	}

	@Override
	public Object value(Float value) {
		return new JSONBase(value);
	}

	@Override
	public Object value(Long value) {
		return new JSONBase(value);
	}

	@Override
	public Object value(Short value) {
		return new JSONBase(value);
	}

	@Override
	public Object value(Byte value) {
		return new JSONBase(value);
	}

	@Override
	public Object value(Boolean value) {
		return new JSONBase(value);
	}

	@Override
	public Object value(Character value) {
		return new JSONBase(value.toString());
	}

	@Override
	public Object value(Date value) {
		return new JSONBase(value);
	}

	@Override
	public Object value(BigDecimal value) {
		return new JSONBase(value);
	}

	@Override
	public Object value(BigInteger value) {
		return new JSONBase(value);
	}

	@Override
	public Object value(JSONBase value) {
		return value;
	}
}
