package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ArrayDeserializerForCollection implements JSONArrayDeserializer {

	private Collection<Object>	collection;

	private JSONDeserializer	deserializer;

	public ArrayDeserializerForCollection(Collection<Object> collection, JSONDeserializer deserializer) {
		super();
		this.collection = collection;
		this.deserializer = deserializer;
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return deserializer.createArrayDeserializer();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return deserializer.createObjectDeserializer();
	}

	@Override
	public void addValue(Object value) {
		collection.add(value);
	}

	@Override
	public void addStringValue(JSONLexer jsonLexer) {
		collection.add(deserializer.stringValue(jsonLexer));
	}

	@Override
	public void addIntValue(String value) {
		collection.add(deserializer.intValue(value));
	}

	@Override
	public void addFloatValue(String value) {
		collection.add(deserializer.floatValue(value));
	}

	@Override
	public void addFalseValue() {
		collection.add(deserializer.falseValue());
	}

	@Override
	public void addTrueValue() {
		collection.add(deserializer.trueValue());
	}

	@Override
	public void addNullValue() {
		collection.add(null);
	}

	@Override
	public Object getDeserializedValue() {
		return collection;
	}

	@Override
	public void addValue(Integer value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(Double value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(Float value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(Long value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(Short value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(Byte value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(Boolean value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(Character value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(Date value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(BigDecimal value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(BigInteger value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(JSONBase value) {
		collection.add(deserializer.value(value));
	}

	@Override
	public void addValue(String value) {
		collection.add(deserializer.stringValue(value));
	}

	@Override
	public Object to(int[] value) {
		for (int item : value) {
			collection.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(double[] value) {
		for (double item : value) {
			collection.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(float[] value) {
		for (float item : value) {
			collection.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(long[] value) {
		for (long item : value) {
			collection.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(short[] value) {
		for (short item : value) {
			collection.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(byte[] value) {
		for (byte item : value) {
			collection.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(boolean[] value) {
		for (boolean item : value) {
			collection.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

	@Override
	public Object to(char[] value) {
		for (char item : value) {
			collection.add(deserializer.value(item));
		}
		return getDeserializedValue();
	}

}
