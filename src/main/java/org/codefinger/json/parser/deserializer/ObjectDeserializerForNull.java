package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ObjectDeserializerForNull implements JSONObjectDeserializer {

	public static final ObjectDeserializerForNull	INSTANCE	= new ObjectDeserializerForNull();

	private ObjectDeserializerForNull() {

	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return INSTANCE;
	}

	@Override
	public void setFieldName(JSONLexer jsonLexer) {
		jsonLexer.skipCurrentString();
	}

	@Override
	public void setFieldName(String fieldName) {

	}

	@Override
	public void setValue(Object value) {

	}

	@Override
	public void setStringValue(JSONLexer jsonLexer) {
		jsonLexer.skipCurrentString();
	}

	@Override
	public void setIntValue(String value) {

	}

	@Override
	public void setFloatValue(String value) {

	}

	@Override
	public void setFalseValue() {

	}

	@Override
	public void setTrueValue() {

	}

	@Override
	public Object getDeserializedValue() {
		return null;
	}

	@Override
	public void setValue(Integer value) {

	}

	@Override
	public void setValue(Double value) {

	}

	@Override
	public void setValue(Float value) {

	}

	@Override
	public void setValue(Long value) {

	}

	@Override
	public void setValue(Short value) {

	}

	@Override
	public void setValue(Byte value) {

	}

	@Override
	public void setValue(Boolean value) {

	}

	@Override
	public void setValue(Character value) {

	}

	@Override
	public void setValue(Date value) {

	}

	@Override
	public void setValue(BigDecimal value) {

	}

	@Override
	public void setValue(BigInteger value) {

	}

	@Override
	public void setValue(JSONBase value) {

	}

	@Override
	public void setValue(String value) {

	}

}
