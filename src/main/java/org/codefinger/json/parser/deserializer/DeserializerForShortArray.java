package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForShortArray implements JSONDeserializer {

	public static final DeserializerForShortArray	INSTANCE	= new DeserializerForShortArray();

	private DeserializerForShortArray() {

	}

	@Override
	public Object stringValue(String value) {
		return new short[] { Short.parseShort(value) };
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return new short[] { Short.parseShort(jsonLexer.getOriginalString()) };
	}

	@Override
	public Object intValue(String value) {
		return new short[] { Short.parseShort(value) };
	}

	@Override
	public Object floatValue(String value) {
		return new short[] { new BigDecimal(value).shortValue() };
	}

	@Override
	public Object falseValue() {
		return new short[] { 0 };
	}

	@Override
	public Object trueValue() {
		return new short[] { 1 };
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForShortArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return new short[] { value.shortValue() };
	}

	@Override
	public Object value(Double value) {
		return new short[] { value.shortValue() };
	}

	@Override
	public Object value(Float value) {
		return new short[] { value.shortValue() };
	}

	@Override
	public Object value(Long value) {
		return new short[] { value.shortValue() };
	}

	@Override
	public Object value(Short value) {
		return new short[] { value };
	}

	@Override
	public Object value(Byte value) {
		return new short[] { value.shortValue() };
	}

	@Override
	public Object value(Boolean value) {
		return new short[] { value ? (short) 1 : 0 };
	}

	@Override
	public Object value(Character value) {
		return new short[] { (short) value.charValue() };
	}

	@Override
	public Object value(Date value) {
		return new short[] { (short) value.getTime() };
	}

	@Override
	public Object value(BigDecimal value) {
		return new short[] { value.shortValue() };
	}

	@Override
	public Object value(BigInteger value) {
		return new short[] { value.shortValue() };
	}

	@Override
	public Object value(JSONBase value) {
		return new short[] { value.shortValue() };
	}

}
