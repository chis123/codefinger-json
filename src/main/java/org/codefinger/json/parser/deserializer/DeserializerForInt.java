package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForInt implements JSONDeserializer {

	public static final DeserializerForInt	INSTANCE	= new DeserializerForInt();

	private DeserializerForInt() {

	}

	@Override
	public Object stringValue(String value) {
		return Integer.valueOf(value);
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return Integer.valueOf(jsonLexer.getOriginalString());
	}

	@Override
	public Object intValue(String value) {
		return Integer.valueOf(value);
	}

	@Override
	public Object floatValue(String value) {
		return Integer.valueOf(new BigDecimal(value).intValue());
	}

	@Override
	public Object falseValue() {
		return Integer.valueOf(0);
	}

	@Override
	public Object trueValue() {
		return Integer.valueOf(1);
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return ArrayDeserializerForNull.INSTANCE;
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return ObjectDeserializerForNull.INSTANCE;
	}

	@Override
	public Object value(Integer value) {
		return value;
	}

	@Override
	public Object value(Double value) {
		return value.intValue();
	}

	@Override
	public Object value(Float value) {
		return value.intValue();
	}

	@Override
	public Object value(Long value) {
		return value.intValue();
	}

	@Override
	public Object value(Short value) {
		return value.intValue();
	}

	@Override
	public Object value(Byte value) {
		return value.intValue();
	}

	@Override
	public Object value(Boolean value) {
		return value ? 1 : 0;
	}

	@Override
	public Object value(Character value) {
		return (int) value;
	}

	@Override
	public Object value(Date value) {
		return (int) value.getTime();
	}

	@Override
	public Object value(BigDecimal value) {
		return value.intValue();
	}

	@Override
	public Object value(BigInteger value) {
		return value.intValue();
	}

	@Override
	public Object value(JSONBase value) {
		return value.intValue();
	}

}
