package org.codefinger.json.parser.deserializer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONLexer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class DeserializerForObject implements JSONDeserializer {

	public static final DeserializerForObject	INSTANCE	= new DeserializerForObject();

	private DeserializerForObject() {

	}

	@Override
	public Object stringValue(String value) {
		return value;
	}

	@Override
	public Object stringValue(JSONLexer jsonLexer) {
		return jsonLexer.getJSONString();
	}

	@Override
	public Object intValue(String value) {
		return new BigDecimal(value);
	}

	@Override
	public Object floatValue(String value) {
		return new BigDecimal(value);
	}

	@Override
	public Object falseValue() {
		return Boolean.FALSE;
	}

	@Override
	public Object trueValue() {
		return Boolean.TRUE;
	}

	@Override
	public JSONArrayDeserializer createArrayDeserializer() {
		return new ArrayDeserializerForJSONArray();
	}

	@Override
	public JSONObjectDeserializer createObjectDeserializer() {
		return new ObjectDeserializerForJSONObject();
	}

	@Override
	public Object value(Integer value) {
		return value;
	}

	@Override
	public Object value(Double value) {
		return value;
	}

	@Override
	public Object value(Float value) {
		return value;
	}

	@Override
	public Object value(Long value) {
		return value;
	}

	@Override
	public Object value(Short value) {
		return value;
	}

	@Override
	public Object value(Byte value) {
		return value;
	}

	@Override
	public Object value(Boolean value) {
		return value;
	}

	@Override
	public Object value(Character value) {
		return value;
	}

	@Override
	public Object value(Date value) {
		return value;
	}

	@Override
	public Object value(BigDecimal value) {
		return value;
	}

	@Override
	public Object value(BigInteger value) {
		return value;
	}

	@Override
	public Object value(JSONBase value) {
		return value;
	}
}
