package org.codefinger.json.parser;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;

public interface JSONDeserializer {

	Object stringValue(JSONLexer jsonLexer);

	Object stringValue(String value);

	Object intValue(String value);

	Object floatValue(String value);

	Object falseValue();

	Object trueValue();

	JSONArrayDeserializer createArrayDeserializer();

	JSONObjectDeserializer createObjectDeserializer();

	Object value(Integer value);

	Object value(Double value);

	Object value(Float value);

	Object value(Long value);

	Object value(Short value);

	Object value(Byte value);

	Object value(Boolean value);

	Object value(Character value);

	Object value(Date value);

	Object value(BigDecimal value);

	Object value(BigInteger value);

	Object value(JSONBase value);

}
