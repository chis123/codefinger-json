package org.codefinger.json.parser;

public class JSONParseException extends RuntimeException {

	private static final long	serialVersionUID	= 5536250216701899635L;

	public JSONParseException() {
		super();
	}

	public JSONParseException(String message) {
		super(message);
	}

}
