package org.codefinger.json.parser.converter;

import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONConverter;
import org.codefinger.json.parser.JSONConverterType;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ConverterFromLongArray implements JSONConverter {

	public static final ConverterFromLongArray	INSTANCE	= new ConverterFromLongArray();

	private ConverterFromLongArray() {

	}

	@Override
	public Object convert(Object value, JSONDeserializer deserializer) {
		return null;
	}

	@Override
	public Object convert(Object value, JSONObjectDeserializer objectDeserializer) {
		return null;
	}

	@Override
	public Object convert(Object value, JSONArrayDeserializer arrayDeserializer) {
		return arrayDeserializer.to((long[]) value);
	}

	@Override
	public void setItemValue(Object value, JSONObjectDeserializer objectDeserializer) {
	}

	@Override
	public void addItemValue(Object value, JSONArrayDeserializer arrayDeserializer) {
	}

	@Override
	public JSONConverterType jsonConverterType() {
		return JSONConverterType.Array;
	}

}
