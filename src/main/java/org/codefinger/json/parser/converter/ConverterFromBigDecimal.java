package org.codefinger.json.parser.converter;

import java.math.BigDecimal;

import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONConverter;
import org.codefinger.json.parser.JSONConverterType;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONObjectDeserializer;

public class ConverterFromBigDecimal implements JSONConverter {

	public static final ConverterFromBigDecimal	INSTANCE	= new ConverterFromBigDecimal();

	private ConverterFromBigDecimal() {

	}

	@Override
	public Object convert(Object value, JSONDeserializer deserializer) {
		return deserializer.value((BigDecimal) value);
	}

	@Override
	public Object convert(Object value, JSONObjectDeserializer objectDeserializer) {
		return null;
	}

	@Override
	public Object convert(Object value, JSONArrayDeserializer arrayDeserializer) {
		return null;
	}

	@Override
	public void setItemValue(Object value, JSONObjectDeserializer objectDeserializer) {
		objectDeserializer.setValue((BigDecimal) value);
	}

	@Override
	public void addItemValue(Object value, JSONArrayDeserializer arrayDeserializer) {
		arrayDeserializer.addValue((BigDecimal) value);
	}

	@Override
	public JSONConverterType jsonConverterType() {
		return JSONConverterType.Base;
	}

}
