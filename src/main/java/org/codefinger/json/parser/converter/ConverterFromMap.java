package org.codefinger.json.parser.converter;

import java.util.Map;
import java.util.Map.Entry;

import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONConverter;
import org.codefinger.json.parser.JSONConverterType;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONObjectDeserializer;
import org.codefinger.json.parser.JSONParser;

public class ConverterFromMap implements JSONConverter {

	public static final ConverterFromMap	INSTANCE	= new ConverterFromMap();

	private ConverterFromMap() {

	}

	@Override
	public Object convert(Object value, JSONDeserializer deserializer) {
		return null;
	}

	@Override
	public Object convert(Object value, JSONObjectDeserializer objectDeserializer) {
		Map<?, ?> map = (Map<?, ?>) value;
		for (Entry<?, ?> entry : map.entrySet()) {
			Object val = entry.getValue();
			if (val == null) {
				continue;
			}
			objectDeserializer.setFieldName(entry.getKey().toString());
			JSONParser.setItem(val, objectDeserializer);
		}
		return objectDeserializer.getDeserializedValue();
	}

	@Override
	public Object convert(Object value, JSONArrayDeserializer arrayDeserializer) {
		return null;
	}

	@Override
	public void setItemValue(Object value, JSONObjectDeserializer objectDeserializer) {

	}

	@Override
	public void addItemValue(Object value, JSONArrayDeserializer arrayDeserializer) {

	}

	@Override
	public JSONConverterType jsonConverterType() {
		return JSONConverterType.Object;
	}

}
