package org.codefinger.json.parser.converter;

import org.codefinger.json.parser.JSONArrayDeserializer;
import org.codefinger.json.parser.JSONConverter;
import org.codefinger.json.parser.JSONConverterType;
import org.codefinger.json.parser.JSONDeserializer;
import org.codefinger.json.parser.JSONObjectDeserializer;
import org.codefinger.json.parser.JSONParser;

public class ConverterFromArray implements JSONConverter {

	public static final ConverterFromArray	INSTANCE	= new ConverterFromArray();

	private ConverterFromArray() {

	}

	@Override
	public Object convert(Object value, JSONDeserializer deserializer) {
		return null;
	}

	@Override
	public Object convert(Object value, JSONObjectDeserializer objectDeserializer) {
		return null;
	}

	@Override
	public Object convert(Object value, JSONArrayDeserializer arrayDeserializer) {
		Object[] array = (Object[]) value;
		for (Object item : array) {
			if (item == null) {
				arrayDeserializer.addNullValue();
				continue;
			}
			JSONParser.addItem(item, arrayDeserializer);
		}
		return arrayDeserializer.getDeserializedValue();
	}

	@Override
	public void setItemValue(Object value, JSONObjectDeserializer objectDeserializer) {

	}

	@Override
	public void addItemValue(Object value, JSONArrayDeserializer arrayDeserializer) {

	}

	@Override
	public JSONConverterType jsonConverterType() {
		return JSONConverterType.Array;
	}

}
