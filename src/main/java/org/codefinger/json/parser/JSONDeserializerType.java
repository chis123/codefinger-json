package org.codefinger.json.parser;

public enum JSONDeserializerType {

	Null, Object, Array

}
