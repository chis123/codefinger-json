package org.codefinger.json;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.codefinger.json.parser.JSONParser;
import org.codefinger.json.util.type.TypeUtil;

public final class JSONArray extends ArrayList<Object> implements JSON {

	private static final long	serialVersionUID	= 2011134343918737941L;

	public JSONArray() {
		super();
	}

	public JSONArray(int initialCapaticy) {
		super(initialCapaticy);
	}

	public JSONArray(Collection<? extends Object> collection) {
		super(collection);
	}

	public Integer getInt(int index) {
		return TypeUtil.castToInt(get(index));
	}

	public Short getShort(int index) {
		return TypeUtil.castToShort(get(index));
	}

	public Double getDouble(int index) {
		return TypeUtil.castToDouble(get(index));
	}

	public Float getFloat(int index) {
		return TypeUtil.castToFloat(get(index));
	}

	public Character getChar(int index) {
		return TypeUtil.castToChar(get(index));
	}

	public Long getLong(int index) {
		return TypeUtil.castToLong(get(index));
	}

	public Byte getByte(int index) {
		return TypeUtil.castToByte(get(index));
	}

	public Boolean getBoolean(int index) {
		return TypeUtil.castToBoolean(get(index));
	}

	public String getString(int index) {
		return TypeUtil.castToString(get(index));
	}

	public BigDecimal getBigDecimal(int index) {
		return TypeUtil.castToBigDecimal(get(index));
	}

	public BigInteger getBigInteger(int index) {
		return TypeUtil.castToBigInteger(get(index));
	}

	public Date getDate(int index) {
		return TypeUtil.castToDate(get(index));
	}

	@SuppressWarnings("unchecked")
	public <T> T toJavaObject(Class<T> clazz) {
		return (T) JSONParser.parseTo(this, clazz);
	}

	@SuppressWarnings("unchecked")
	public <T> T toJavaObject(Type type) {
		return (T) JSONParser.parseTo(this, type);
	}

	@Override
	public JSONType jsonType() {
		return JSONType.Array;
	}

	@Override
	public String toString() {
		return JSONUtil.toJSONString(this);
	}

}
