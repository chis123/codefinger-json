package org.codefinger.json.util.type;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.JSONBase;
import org.codefinger.json.util.type.converter.BigDecimalConverter;
import org.codefinger.json.util.type.converter.BigIntegerConverter;
import org.codefinger.json.util.type.converter.BooleanConverter;
import org.codefinger.json.util.type.converter.ByteConverter;
import org.codefinger.json.util.type.converter.CharConverter;
import org.codefinger.json.util.type.converter.DateConverter;
import org.codefinger.json.util.type.converter.DoubleConverter;
import org.codefinger.json.util.type.converter.FloatConverter;
import org.codefinger.json.util.type.converter.IntConverter;
import org.codefinger.json.util.type.converter.JSONBaseConverter;
import org.codefinger.json.util.type.converter.LongConverter;
import org.codefinger.json.util.type.converter.ShortConverter;
import org.codefinger.json.util.type.converter.StringConverter;

public final class TypeUtil {

	private TypeUtil() {

	}

	public static Integer castToInt(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof Integer) {
			return (Integer) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return null;
		}

		return converter.castToInt(value);
	}

	public static Character castToChar(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof Character) {
			return (Character) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return null;
		}

		return converter.castToChar(value);
	}

	public static Byte castToByte(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof Byte) {
			return (Byte) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return null;
		}

		return converter.castToByte(value);
	}

	public static Short castToShort(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof Short) {
			return (Short) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return null;
		}

		return converter.castToShort(value);
	}

	public static Long castToLong(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof Long) {
			return (Long) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return null;
		}

		return converter.castToLong(value);
	}

	public static Float castToFloat(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof Float) {
			return (Float) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return null;
		}

		return converter.castToFloat(value);
	}

	public static Double castToDouble(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof Double) {
			return (Double) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return null;
		}

		return converter.castToDouble(value);
	}

	public static Boolean castToBoolean(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof Boolean) {
			return (Boolean) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return null;
		}

		return converter.castToBoolean(value);
	}

	public static String castToString(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof String) {
			return (String) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return value.toString();
		}

		return converter.castToString(value);
	}

	public static BigDecimal castToBigDecimal(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof BigDecimal) {
			return (BigDecimal) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return null;
		}

		return converter.castToBigDecimal(value);
	}

	public static BigInteger castToBigInteger(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof BigInteger) {
			return (BigInteger) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return null;
		}

		return converter.castToBigInteger(value);
	}

	public static Date castToDate(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof Date) {
			return (Date) value;
		}

		TypeConverter converter = getTypeConverter(value.getClass());

		if (converter == null) {
			return null;
		}

		return converter.castToDate(value);
	}

	public static TypeConverter getTypeConverter(Class<?> clazz) {
		if (clazz == String.class) {
			return StringConverter.INSTANCE;
		}
		if (clazz == Integer.class) {
			return IntConverter.INSTANCE;
		}
		if (clazz == Long.class) {
			return LongConverter.INSTANCE;
		}
		if (clazz == Boolean.class) {
			return BooleanConverter.INSTANCE;
		}
		if (clazz == Double.class) {
			return DoubleConverter.INSTANCE;
		}
		if (Date.class.isAssignableFrom(clazz)) {
			return DateConverter.INSTANCE;
		}
		if (clazz == BigDecimal.class) {
			return BigDecimalConverter.INSTANCE;
		}
		if (clazz == Float.class) {
			return FloatConverter.INSTANCE;
		}
		if (clazz == BigInteger.class) {
			return BigIntegerConverter.INSTANCE;
		}
		if (clazz == JSONBase.class) {
			return JSONBaseConverter.INSTANCE;
		}
		if (clazz == Byte.class) {
			return ByteConverter.INSTANCE;
		}
		if (clazz == Character.class) {
			return CharConverter.INSTANCE;
		}
		if (clazz == Short.class) {
			return ShortConverter.INSTANCE;
		}
		return null;
	}
}
