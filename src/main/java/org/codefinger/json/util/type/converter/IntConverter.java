package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.TypeConverter;

public class IntConverter implements TypeConverter {

	public static final IntConverter	INSTANCE	= new IntConverter();

	private IntConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return (Integer) value;
	}

	@Override
	public Byte castToByte(Object value) {
		return ((Integer) value).byteValue();
	}

	@Override
	public Character castToChar(Object value) {
		return (char) ((Integer) value).intValue();
	}

	@Override
	public Short castToShort(Object value) {
		return ((Integer) value).shortValue();
	}

	@Override
	public Long castToLong(Object value) {
		return ((Integer) value).longValue();
	}

	@Override
	public Float castToFloat(Object value) {
		return ((Integer) value).floatValue();
	}

	@Override
	public Double castToDouble(Object value) {
		return ((Integer) value).doubleValue();
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return (Integer) value == 0 ? false : true;
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return new BigDecimal(((Integer) value));
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return new BigInteger(value.toString());
	}

	@Override
	public Date castToDate(Object value) {
		return new Date(((Integer) value).longValue());
	}

}
