package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.TypeConverter;

public class ShortConverter implements TypeConverter {

	public static final ShortConverter	INSTANCE	= new ShortConverter();

	private ShortConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return ((Short) value).intValue();
	}

	@Override
	public Byte castToByte(Object value) {
		return ((Short) value).byteValue();
	}

	@Override
	public Character castToChar(Object value) {
		return (char) ((Short) value).intValue();
	}

	@Override
	public Short castToShort(Object value) {
		return (Short) value;
	}

	@Override
	public Long castToLong(Object value) {
		return ((Short) value).longValue();
	}

	@Override
	public Float castToFloat(Object value) {
		return ((Short) value).floatValue();
	}

	@Override
	public Double castToDouble(Object value) {
		return ((Short) value).doubleValue();
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return (Short) value == 0 ? false : true;
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return new BigDecimal(value.toString());
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return new BigInteger(value.toString());
	}

	@Override
	public Date castToDate(Object value) {
		return new Date(((Short) value).longValue());
	}

}
