package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.TypeConverter;

public class StringConverter implements TypeConverter {

	public static final StringConverter	INSTANCE	= new StringConverter();

	private StringConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		try {
			return Integer.valueOf(value.toString());
		} catch (Throwable throwable) {
			return null;
		}
	}

	@Override
	public Byte castToByte(Object value) {
		try {
			return Byte.valueOf(value.toString());
		} catch (Throwable throwable) {
			return null;
		}
	}

	@Override
	public Character castToChar(Object value) {
		String str = value.toString();
		if (str.length() > 0) {
			return str.charAt(0);
		}
		return null;
	}

	@Override
	public Short castToShort(Object value) {
		try {
			return Short.valueOf(value.toString());
		} catch (Throwable throwable) {
			return null;
		}
	}

	@Override
	public Long castToLong(Object value) {
		try {
			return Long.valueOf(value.toString());
		} catch (Throwable throwable) {
			return null;
		}
	}

	@Override
	public Float castToFloat(Object value) {
		try {
			return Float.valueOf(value.toString());
		} catch (Throwable throwable) {
			return null;
		}
	}

	@Override
	public Double castToDouble(Object value) {
		try {
			return Double.valueOf(value.toString());
		} catch (Throwable throwable) {
			return null;
		}
	}

	@Override
	public Boolean castToBoolean(Object value) {
		try {
			return Boolean.valueOf(value.toString());
		} catch (Throwable throwable) {
			return null;
		}
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return new BigDecimal(value.toString());
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return new BigInteger(value.toString());
	}

	@Override
	public Date castToDate(Object value) {
		return new Date(((Short) value).longValue());
	}

}
