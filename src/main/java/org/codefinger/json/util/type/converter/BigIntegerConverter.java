package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.TypeConverter;

public class BigIntegerConverter implements TypeConverter {

	public static final BigIntegerConverter	INSTANCE	= new BigIntegerConverter();

	private BigIntegerConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return ((BigInteger) value).intValue();
	}

	@Override
	public Byte castToByte(Object value) {
		return ((BigInteger) value).byteValue();
	}

	@Override
	public Character castToChar(Object value) {
		return (char) ((BigInteger) value).intValue();
	}

	@Override
	public Short castToShort(Object value) {
		return ((BigInteger) value).shortValue();
	}

	@Override
	public Long castToLong(Object value) {
		return ((BigInteger) value).longValue();
	}

	@Override
	public Float castToFloat(Object value) {
		return ((BigInteger) value).floatValue();
	}

	@Override
	public Double castToDouble(Object value) {
		return ((BigInteger) value).doubleValue();
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return ((BigInteger) value).intValue() == 0 ? false : true;
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return new BigDecimal(value.toString());
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return (BigInteger) value;
	}

	@Override
	public Date castToDate(Object value) {
		return new Date(((BigInteger) value).longValue());
	}

}
