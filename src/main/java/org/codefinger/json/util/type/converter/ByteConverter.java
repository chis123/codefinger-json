package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.TypeConverter;

public class ByteConverter implements TypeConverter {

	public static final ByteConverter	INSTANCE	= new ByteConverter();

	private ByteConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return ((Byte) value).intValue();
	}

	@Override
	public Byte castToByte(Object value) {
		return (Byte) value;
	}

	@Override
	public Character castToChar(Object value) {
		return (char) ((Byte) value).intValue();
	}

	@Override
	public Short castToShort(Object value) {
		return ((Byte) value).shortValue();
	}

	@Override
	public Long castToLong(Object value) {
		return ((Byte) value).longValue();
	}

	@Override
	public Float castToFloat(Object value) {
		return ((Byte) value).floatValue();
	}

	@Override
	public Double castToDouble(Object value) {
		return ((Byte) value).doubleValue();
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return (Byte) value == 0 ? false : true;
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return new BigDecimal(value.toString());
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return new BigInteger(value.toString());
	}

	@Override
	public Date castToDate(Object value) {
		return new Date(((Byte) value).longValue());
	}

}
