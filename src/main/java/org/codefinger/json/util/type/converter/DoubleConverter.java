package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.TypeConverter;

public class DoubleConverter implements TypeConverter {

	public static final DoubleConverter	INSTANCE	= new DoubleConverter();

	private DoubleConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return ((Double) value).intValue();
	}

	@Override
	public Byte castToByte(Object value) {
		return ((Double) value).byteValue();
	}

	@Override
	public Character castToChar(Object value) {
		return (char) ((Double) value).intValue();
	}

	@Override
	public Short castToShort(Object value) {
		return ((Double) value).shortValue();
	}

	@Override
	public Long castToLong(Object value) {
		return ((Double) value).longValue();
	}

	@Override
	public Float castToFloat(Object value) {
		return ((Double) value).floatValue();
	}

	@Override
	public Double castToDouble(Object value) {
		return (Double) value;
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return (Double) value == 0 ? false : true;
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return new BigDecimal(value.toString());
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return BigInteger.valueOf(((Double) value).longValue());
	}

	@Override
	public Date castToDate(Object value) {
		return new Date(((Double) value).longValue());
	}

}
