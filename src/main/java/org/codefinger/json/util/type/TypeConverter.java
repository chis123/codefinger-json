package org.codefinger.json.util.type;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public interface TypeConverter {

	Integer castToInt(Object value);

	Byte castToByte(Object value);

	Character castToChar(Object value);

	Short castToShort(Object value);

	Long castToLong(Object value);

	Float castToFloat(Object value);

	Double castToDouble(Object value);

	Boolean castToBoolean(Object value);

	String castToString(Object value);

	BigDecimal castToBigDecimal(Object value);

	BigInteger castToBigInteger(Object value);

	Date castToDate(Object value);

}
