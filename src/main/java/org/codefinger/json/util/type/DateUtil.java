package org.codefinger.json.util.type;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private static final DateFormat[]	DATE_FORMATS	= new DateFormat[] { new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS"), new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"), new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSXXX"), new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssXXX"), new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS"), new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss"), new SimpleDateFormat("yyyy-MM-dd") };

	public static Date parseDate(String date) {
		try {
			return new Date(Long.valueOf(date));
		} catch (Throwable throwable) {
			for (DateFormat dateFormat : DATE_FORMATS) {
				try {
					return dateFormat.parse(date);
				} catch (Throwable t) {
					continue;
				}
			}
		}
		return null;
	}

	public static String formatSimple(Date date) {
		return DATE_FORMATS[0].format(date);
	}

}
