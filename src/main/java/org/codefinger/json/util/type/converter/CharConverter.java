package org.codefinger.json.util.type.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codefinger.json.util.type.DateUtil;
import org.codefinger.json.util.type.TypeConverter;

public class CharConverter implements TypeConverter {

	public static final CharConverter	INSTANCE	= new CharConverter();

	private CharConverter() {

	}

	@Override
	public Integer castToInt(Object value) {
		return (int) ((Character) value).charValue();
	}

	@Override
	public Byte castToByte(Object value) {
		return (byte) ((Character) value).charValue();
	}

	@Override
	public Character castToChar(Object value) {
		return (Character) value;
	}

	@Override
	public Short castToShort(Object value) {
		return (short) ((Character) value).charValue();
	}

	@Override
	public Long castToLong(Object value) {
		return (long) ((Character) value).charValue();
	}

	@Override
	public Float castToFloat(Object value) {
		return (float) ((Character) value).charValue();
	}

	@Override
	public Double castToDouble(Object value) {
		return (double) ((Character) value).charValue();
	}

	@Override
	public Boolean castToBoolean(Object value) {
		return ((Character) value).charValue() == 0 ? false : true;
	}

	@Override
	public String castToString(Object value) {
		return value.toString();
	}

	@Override
	public BigDecimal castToBigDecimal(Object value) {
		return new BigDecimal(((Character) value).charValue());
	}

	@Override
	public BigInteger castToBigInteger(Object value) {
		return BigInteger.valueOf(((Character) value).charValue());
	}

	@Override
	public Date castToDate(Object value) {
		return DateUtil.parseDate(value.toString());
	}

}
