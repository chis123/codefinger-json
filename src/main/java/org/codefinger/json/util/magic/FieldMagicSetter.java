package org.codefinger.json.util.magic;

import java.lang.reflect.Field;

public class FieldMagicSetter implements MagicSetter {

	private Field	field;

	FieldMagicSetter(Field field) {
		field.setAccessible(true);
		this.field = field;
	}

	@Override
	public void setValue(Object object, Object value) {
		try {
			field.set(object, value);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
