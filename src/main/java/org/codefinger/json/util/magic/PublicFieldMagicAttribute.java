package org.codefinger.json.util.magic;

import java.lang.reflect.Field;

public class PublicFieldMagicAttribute implements MagicAttribute {

	private Field		field;

	private String		fieldName;

	private MagicField	magicField;

	PublicFieldMagicAttribute(Field field) {
		this.field = field;
		this.fieldName = field.getName();
		this.magicField = MagicField.getMagicField(field);
	}

	@Override
	public Field getField() {
		return field;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Override
	public void setValue(Object object, Object value) {
		magicField.setValue(object, value);
	}

	@Override
	public Object getValue(Object object) {
		return magicField.getValue(object);
	}
}
