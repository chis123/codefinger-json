package org.codefinger.json.util.magic;

import java.lang.reflect.Field;

import org.codefinger.json.util.Lang;

public class FieldMagicGetter implements MagicGetter {

	private Field	field;

	FieldMagicGetter(Field field) {
		field.setAccessible(true);
		this.field = field;
	}

	@Override
	public Object getValue(Object object) {
		try {
			return field.get(object);
		} catch (Throwable throwable) {
			throw Lang.wrapThrow(throwable, "Get value error!");
		}
	}

}
