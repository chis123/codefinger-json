package org.codefinger.json.serializer;

import org.codefinger.json.JSONBase;

public class SerializerForJSONBase implements JSONSerializer {

	public static SerializerForJSONBase	INSTANCE	= new SerializerForJSONBase();

	private SerializerForJSONBase() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		JSONBase jsonBase = (JSONBase) value;
		switch (jsonBase.jsonType()) {
		case Boolean:
			writer.writeBoolean(jsonBase.booleanValue());
			break;
		case Date:
			writer.writeLong(jsonBase.dateValue().getTime());
			break;
		case Number:
			writer.write(jsonBase.bigDecimalValue().toString());
			break;
		default:
			writer.writeString(jsonBase.toString());
		}
	}

}
