package org.codefinger.json.serializer;

import java.util.Iterator;

public class SerializerForCollection implements JSONSerializer {

	public static SerializerForCollection	INSTANCE	= new SerializerForCollection();

	private SerializerForCollection() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		Iterator<?> iterator = ((Iterable<?>) value).iterator();
		writer.write('[');
		if (iterator.hasNext()) {
			JSONSerializerFactory.write(writer, iterator.next());
		}
		while (iterator.hasNext()) {
			writer.write(',');
			JSONSerializerFactory.write(writer, iterator.next());
		}
		writer.write(']');
	}

}
