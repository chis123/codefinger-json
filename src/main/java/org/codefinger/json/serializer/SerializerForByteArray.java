package org.codefinger.json.serializer;

public class SerializerForByteArray implements JSONSerializer {

	public static SerializerForByteArray	INSTANCE	= new SerializerForByteArray();

	private SerializerForByteArray() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.writeByteArray((byte[]) value);
	}
}
