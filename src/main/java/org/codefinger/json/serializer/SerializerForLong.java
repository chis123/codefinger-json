package org.codefinger.json.serializer;

public class SerializerForLong implements JSONSerializer {

	public static SerializerForLong	INSTANCE	= new SerializerForLong();

	private SerializerForLong() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.writeLong((Long) value);
	}

}
