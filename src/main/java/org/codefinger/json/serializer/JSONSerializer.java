package org.codefinger.json.serializer;

public interface JSONSerializer {

	void writeJSONString(SerializerWriter writer, Object value);

}