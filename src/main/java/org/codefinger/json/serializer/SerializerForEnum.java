package org.codefinger.json.serializer;

public class SerializerForEnum implements JSONSerializer {

	public static SerializerForEnum	INSTANCE	= new SerializerForEnum();

	private SerializerForEnum() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.writeString(((Enum<?>) value).name());
	}

}
