package org.codefinger.json.serializer;

import java.util.Map;
import java.util.Map.Entry;

public class SerializerForMap implements JSONSerializer {

	public static final SerializerForMap	INSTANCE	= new SerializerForMap();

	private SerializerForMap() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		Map<?, ?> map = (Map<?, ?>) value;
		writer.write('{');
		boolean flag = false;
		for (Entry<?, ?> entry : map.entrySet()) {
			Object val = entry.getValue();
			if (val == null) {
				continue;
			}
			if (flag) {
				writer.write(',');
			} else {
				flag = true;
			}
			writer.writeString(entry.getKey().toString());
			writer.write(':');
			JSONSerializerFactory.write(writer, val);
		}
		writer.write('}');
	}

}
