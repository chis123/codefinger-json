package org.codefinger.json.serializer;

public class SerializerForDoubleArray implements JSONSerializer {

	public static SerializerForDoubleArray	INSTANCE	= new SerializerForDoubleArray();

	private SerializerForDoubleArray() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		double[] i = (double[]) value;
		writer.write('[');
		int length = i.length;
		if (length > 0) {
			writer.write(Double.toString(i[0]));
			for (int j = 1; j < length; j++) {
				writer.write(',');
				writer.write(Double.toString(i[j]));
			}
		}
		writer.write(']');
	}
}
