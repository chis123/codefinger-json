package org.codefinger.json.serializer;

public class SerializerForInt implements JSONSerializer {

	public static SerializerForInt	INSTANCE	= new SerializerForInt();

	private SerializerForInt() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.writeInt((Integer) value);
	}

}
