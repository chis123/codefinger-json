package org.codefinger.json.serializer;

import org.codefinger.json.util.Lang;
import org.codefinger.json.util.magic.MagicAttribute;

public class PojoFieldSerializer {

	private MagicAttribute	magicAttribute;

	private JSONSerializer	serializer;

	private char[]			fieldName;

	public PojoFieldSerializer(MagicAttribute magicAttribute, JSONSerializer serializer) {
		super();
		this.magicAttribute = magicAttribute;
		this.serializer = serializer;
		this.fieldName = Lang.joinString('"', magicAttribute.getFieldName(), "\":").toString().toCharArray();
	}

	public boolean write(SerializerWriter writer, Object target, boolean flag) {
		Object value = magicAttribute.getValue(target);
		if (value == null) {
			return false;
		}
		if (flag) {
			writer.write(',');
		}
		writer.write(fieldName);
		serializer.writeJSONString(writer, value);
		return true;
	}

}
