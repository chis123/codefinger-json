package org.codefinger.json.serializer;

public class SerializerForNumber implements JSONSerializer {

	public static SerializerForNumber	INSTANCE	= new SerializerForNumber();

	private SerializerForNumber() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.write(value.toString());
	}
}
