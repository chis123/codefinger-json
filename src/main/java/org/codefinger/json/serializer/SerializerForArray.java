package org.codefinger.json.serializer;

import org.codefinger.json.util.ArrayIterator;

public class SerializerForArray implements JSONSerializer {

	public static SerializerForArray	INSTANCE	= new SerializerForArray();

	private SerializerForArray() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		ArrayIterator<Object> iterator = new ArrayIterator<Object>((Object[]) value);
		writer.write('[');
		if (iterator.hasNext()) {
			JSONSerializerFactory.write(writer, iterator.next());
		}
		while (iterator.hasNext()) {
			writer.write(',');
			JSONSerializerFactory.write(writer, iterator.next());
		}
		writer.write(']');
	}
}
