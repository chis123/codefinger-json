package org.codefinger.json.serializer;

public class SerializerForCharArray implements JSONSerializer {

	public static SerializerForCharArray	INSTANCE	= new SerializerForCharArray();

	private SerializerForCharArray() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.write('"');
		writer.write((char[]) value);
		writer.write('"');
	}
}
