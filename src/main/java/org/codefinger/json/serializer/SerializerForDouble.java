package org.codefinger.json.serializer;

public class SerializerForDouble implements JSONSerializer {

	public static SerializerForDouble	INSTANCE	= new SerializerForDouble();

	private SerializerForDouble() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		writer.write(Double.toString((Double) value));
	}

}
