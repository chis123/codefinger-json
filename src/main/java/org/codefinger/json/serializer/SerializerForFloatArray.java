package org.codefinger.json.serializer;

public class SerializerForFloatArray implements JSONSerializer {

	public static SerializerForFloatArray	INSTANCE	= new SerializerForFloatArray();

	private SerializerForFloatArray() {

	}

	@Override
	public void writeJSONString(SerializerWriter writer, Object value) {
		float[] i = (float[]) value;
		writer.write('[');
		int length = i.length;
		if (length > 0) {
			writer.write(Float.toString(i[0]));
			for (int j = 1; j < length; j++) {
				writer.write(',');
				writer.write(Float.toString(i[j]));
			}
		}
		writer.write(']');
	}
}
