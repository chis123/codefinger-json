package org.codefinger.json.test;

import org.codefinger.json.JSONUtil;
import org.codefinger.json.util.magic.MagicClassLoader;
import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class TestJSONSerializer {

	/**
	 * 与Fastjson的性能对比
	 */
	@Test
	public void testFast() {
		String jsonText = MagicClassLoader.loadResourceText("org/codefinger/json/test/json.js");
		JSONObject jsonObject = JSONUtil.parse(jsonText, JSONObject.class);

		long begin, end;

		begin = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			JSONUtil.toJSONString(jsonObject);
		}
		end = System.currentTimeMillis();
		System.out.println("Codefinger:" + (end - begin));
		System.out.println("Codefinger:" + JSONUtil.toJSONString(jsonObject));

		begin = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			JSON.toJSONString(jsonObject);
		}
		end = System.currentTimeMillis();
		System.out.println("Fastjson:" + (end - begin));
		System.out.println("Fastjson:" + JSON.toJSONString(jsonObject));

		JSONPojo jsonPojo = JSONUtil.parse(jsonText, JSONPojo.class);

		begin = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			JSON.toJSONString(jsonPojo);
		}
		end = System.currentTimeMillis();
		System.out.println("Fastjson:" + (end - begin));
		System.out.println("Fastjson:" + JSON.toJSONString(jsonPojo));

		begin = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			JSONUtil.toJSONString(jsonPojo);
		}
		end = System.currentTimeMillis();
		System.out.println("Codefinger:" + (end - begin));
		System.out.println("Codefinger:" + JSONUtil.toJSONString(jsonPojo));

	}

}
