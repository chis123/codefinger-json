package org.codefinger.json.test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class JSONPojo {

	private int[]																				intArrayValue;

	private float[]																				floatArrayValue;

	private double[]																			doubleArrayValue;

	private short[]																				shortArrayValue;

	private long[]																				longArrayValue;

	private byte[]																				byteArrayValue;

	private char[]																				charArrayValue;

	private boolean[]																			booleanArrayValue;

	private int																					intValue;

	private float																				floatValue;

	private double																				doubleValue;

	private short																				shortValue;

	private long																				longValue;

	private byte																				byteValue;

	private char																				charValue;

	private boolean																				booleanValue;

	private Integer																				pkgIntValue;

	private Float																				pkgFloatValue;

	private Double																				pkgDoubleValue;

	private Short																				pkgShortValue;

	private Long																				pkgLongValue;

	private Byte																				pkgByteValue;

	private Character																			pkgCharValue;

	private Boolean																				pkgBooleanValue;

	private String																				stringValue;

	private Date																				dateValue;

	private BigDecimal																			bigDecimalValue;

	private BigInteger																			bigIntegerValue;

	private Boolean[]																			pkgBooleanArrayValue;

	private String[]																			stringArrayValue;

	private Date[]																				dateArrayValue;

	private BigDecimal[]																		bigDecimalArrayValue;

	private BigInteger[]																		bigIntegerArrayValue;

	private Map<String, List<Collection<Map<Integer, Map<Double, Map<Date, Character>>[]>>>>	gValue;

	private JSONEnum																			jsonEnum;

	private JSONEnum[]																			jsonEnums;

	public int[] getIntArrayValue() {
		return intArrayValue;
	}

	public void setIntArrayValue(int[] intArrayValue) {
		this.intArrayValue = intArrayValue;
	}

	public float[] getFloatArrayValue() {
		return floatArrayValue;
	}

	public void setFloatArrayValue(float[] floatArrayValue) {
		this.floatArrayValue = floatArrayValue;
	}

	public double[] getDoubleArrayValue() {
		return doubleArrayValue;
	}

	public void setDoubleArrayValue(double[] doubleArrayValue) {
		this.doubleArrayValue = doubleArrayValue;
	}

	public short[] getShortArrayValue() {
		return shortArrayValue;
	}

	public void setShortArrayValue(short[] shortArrayValue) {
		this.shortArrayValue = shortArrayValue;
	}

	public long[] getLongArrayValue() {
		return longArrayValue;
	}

	public void setLongArrayValue(long[] longArrayValue) {
		this.longArrayValue = longArrayValue;
	}

	public byte[] getByteArrayValue() {
		return byteArrayValue;
	}

	public void setByteArrayValue(byte[] byteArrayValue) {
		this.byteArrayValue = byteArrayValue;
	}

	public char[] getCharArrayValue() {
		return charArrayValue;
	}

	public void setCharArrayValue(char[] charArrayValue) {
		this.charArrayValue = charArrayValue;
	}

	public boolean[] getBooleanArrayValue() {
		return booleanArrayValue;
	}

	public void setBooleanArrayValue(boolean[] booleanArrayValue) {
		this.booleanArrayValue = booleanArrayValue;
	}

	public int getIntValue() {
		return intValue;
	}

	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}

	public float getFloatValue() {
		return floatValue;
	}

	public void setFloatValue(float floatValue) {
		this.floatValue = floatValue;
	}

	public double getDoubleValue() {
		return doubleValue;
	}

	public void setDoubleValue(double doubleValue) {
		this.doubleValue = doubleValue;
	}

	public short getShortValue() {
		return shortValue;
	}

	public void setShortValue(short shortValue) {
		this.shortValue = shortValue;
	}

	public long getLongValue() {
		return longValue;
	}

	public void setLongValue(long longValue) {
		this.longValue = longValue;
	}

	public byte getByteValue() {
		return byteValue;
	}

	public void setByteValue(byte byteValue) {
		this.byteValue = byteValue;
	}

	public char getCharValue() {
		return charValue;
	}

	public void setCharValue(char charValue) {
		this.charValue = charValue;
	}

	public boolean isBooleanValue() {
		return booleanValue;
	}

	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}

	public Integer getPkgIntValue() {
		return pkgIntValue;
	}

	public void setPkgIntValue(Integer pkgIntValue) {
		this.pkgIntValue = pkgIntValue;
	}

	public Float getPkgFloatValue() {
		return pkgFloatValue;
	}

	public void setPkgFloatValue(Float pkgFloatValue) {
		this.pkgFloatValue = pkgFloatValue;
	}

	public Double getPkgDoubleValue() {
		return pkgDoubleValue;
	}

	public void setPkgDoubleValue(Double pkgDoubleValue) {
		this.pkgDoubleValue = pkgDoubleValue;
	}

	public Short getPkgShortValue() {
		return pkgShortValue;
	}

	public void setPkgShortValue(Short pkgShortValue) {
		this.pkgShortValue = pkgShortValue;
	}

	public Long getPkgLongValue() {
		return pkgLongValue;
	}

	public void setPkgLongValue(Long pkgLongValue) {
		this.pkgLongValue = pkgLongValue;
	}

	public Byte getPkgByteValue() {
		return pkgByteValue;
	}

	public void setPkgByteValue(Byte pkgByteValue) {
		this.pkgByteValue = pkgByteValue;
	}

	public Character getPkgCharValue() {
		return pkgCharValue;
	}

	public void setPkgCharValue(Character pkgCharValue) {
		this.pkgCharValue = pkgCharValue;
	}

	public Boolean getPkgBooleanValue() {
		return pkgBooleanValue;
	}

	public void setPkgBooleanValue(Boolean pkgBooleanValue) {
		this.pkgBooleanValue = pkgBooleanValue;
	}

	public String getStringValue() {
		return stringValue;
	}

	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}

	public Date getDateValue() {
		return dateValue;
	}

	public void setDateValue(Date dateValue) {
		this.dateValue = dateValue;
	}

	public BigDecimal getBigDecimalValue() {
		return bigDecimalValue;
	}

	public void setBigDecimalValue(BigDecimal bigDecimalValue) {
		this.bigDecimalValue = bigDecimalValue;
	}

	public BigInteger getBigIntegerValue() {
		return bigIntegerValue;
	}

	public void setBigIntegerValue(BigInteger bigIntegerValue) {
		this.bigIntegerValue = bigIntegerValue;
	}

	public Boolean[] getPkgBooleanArrayValue() {
		return pkgBooleanArrayValue;
	}

	public void setPkgBooleanArrayValue(Boolean[] pkgBooleanArrayValue) {
		this.pkgBooleanArrayValue = pkgBooleanArrayValue;
	}

	public String[] getStringArrayValue() {
		return stringArrayValue;
	}

	public void setStringArrayValue(String[] stringArrayValue) {
		this.stringArrayValue = stringArrayValue;
	}

	public Date[] getDateArrayValue() {
		return dateArrayValue;
	}

	public void setDateArrayValue(Date[] dateArrayValue) {
		this.dateArrayValue = dateArrayValue;
	}

	public BigDecimal[] getBigDecimalArrayValue() {
		return bigDecimalArrayValue;
	}

	public void setBigDecimalArrayValue(BigDecimal[] bigDecimalArrayValue) {
		this.bigDecimalArrayValue = bigDecimalArrayValue;
	}

	public BigInteger[] getBigIntegerArrayValue() {
		return bigIntegerArrayValue;
	}

	public void setBigIntegerArrayValue(BigInteger[] bigIntegerArrayValue) {
		this.bigIntegerArrayValue = bigIntegerArrayValue;
	}

	public Map<String, List<Collection<Map<Integer, Map<Double, Map<Date, Character>>[]>>>> getgValue() {
		return gValue;
	}

	public void setgValue(Map<String, List<Collection<Map<Integer, Map<Double, Map<Date, Character>>[]>>>> gValue) {
		this.gValue = gValue;
	}

	public JSONEnum getJsonEnum() {
		return jsonEnum;
	}

	public void setJsonEnum(JSONEnum jsonEnum) {
		this.jsonEnum = jsonEnum;
	}

	public JSONEnum[] getJsonEnums() {
		return jsonEnums;
	}

	public void setJsonEnums(JSONEnum[] jsonEnums) {
		this.jsonEnums = jsonEnums;
	}

}
