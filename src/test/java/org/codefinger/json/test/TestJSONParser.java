package org.codefinger.json.test;

import org.codefinger.json.JSONUtil;
import org.codefinger.json.util.magic.MagicClassLoader;
import org.junit.Test;

import com.alibaba.fastjson.JSON;

public class TestJSONParser {

	/**
	 * 与Fastjson的性能对比
	 */
	@Test
	public void testFast() {
		String jsonText = MagicClassLoader.loadResourceText("org/codefinger/json/test/json.js");

		long begin, end;

		begin = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			JSON.parseObject(jsonText);
		}
		end = System.currentTimeMillis();
		System.out.println("Fastjson:" + (end - begin));

		begin = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			JSONUtil.parseObject(jsonText);
		}
		end = System.currentTimeMillis();
		System.out.println("Codefinger:" + (end - begin));

		begin = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			JSONUtil.parse(jsonText, JSONPojo.class);
		}
		end = System.currentTimeMillis();
		System.out.println("Codefinger:" + (end - begin));

		begin = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			JSON.parseObject(jsonText, JSONPojo.class);
		}
		end = System.currentTimeMillis();
		System.out.println("Fastjson:" + (end - begin));

	}

}
