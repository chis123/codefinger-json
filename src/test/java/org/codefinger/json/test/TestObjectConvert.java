package org.codefinger.json.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.codefinger.json.JSONObject;
import org.codefinger.json.JSONUtil;
import org.codefinger.json.util.magic.MagicClassLoader;
import org.junit.Test;

import com.alibaba.fastjson.JSON;

public class TestObjectConvert {

	/**
	 * 与Fastjson的性能对比
	 * 
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	@Test
	public void testFast() throws FileNotFoundException, UnsupportedEncodingException, IOException {
		String jsonText = MagicClassLoader.loadResourceText("org/codefinger/json/test/json.js");

		JSONObject jsonObject = JSONUtil.parse(jsonText, JSONObject.class);

		com.alibaba.fastjson.JSONObject fastjsonObject = JSON.parseObject(jsonText, com.alibaba.fastjson.JSONObject.class);

		long begin, end;

		// Codefinger能够进行完整转换
		jsonObject.toJavaObject(JSONPojo.class);

		// Fastjson不能进行完整转换，所以为了性能比对，将以下的值去掉
		fastjsonObject.remove("charValue");
		fastjsonObject.remove("charArrayValue");
		fastjsonObject.remove("pkgCharValue");
		fastjsonObject.remove("gValue");

		jsonObject.remove("charValue");
		jsonObject.remove("charArrayValue");
		jsonObject.remove("pkgCharValue");
		jsonObject.remove("gValue");

		begin = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			jsonObject.toJavaObject(JSONPojo.class);
		}
		end = System.currentTimeMillis();
		System.out.println("Codefinger:" + (end - begin));

		begin = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			JSON.toJavaObject(fastjsonObject, JSONPojo.class);
		}
		end = System.currentTimeMillis();
		System.out.println("Fastjson:" + (end - begin));
	}

}
