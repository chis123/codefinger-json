{
	intArrayValue : [ 1123E2, 2, 3, 1e4 ],
	floatArrayValue : [ -123.1E4, -112, 1 ],
	doubleArrayValue : [ -456.5E2, 2, 3 ],
	shortArrayValue : [ 1, 2, 3, 5 ],
	longArrayValue : [ 1233, 12E3, 123, 123 ],
	byteArrayValue : "6L+Z6YeM5piv5LiA5q61QmFzZTY05YaF5a6577yM5aaC5p6c5oKo5bey57uP55yL5Yiw5q2k5a6M5pW05YaF5a6577yI5Lul4oCc44CC4oCd57uT5bC+77yJ77yM6K+B5piO5oKo6Kej56CB5oiQ5Yqf5LqG44CC",
	charArrayValue : "中文Hello\u4E09\u6708",
	booleanArrayValue : [ false, true, false, true ],
	intValue : -1123123,
	floatValue : 1123123,
	doubleValue : 123123,
	shortValue : 123,
	longValue : 12312312312,
	byteValue : 0,
	charValue : 'B',
	booleanValue : true,
	pkgIntValue : 1,
	pkgFloatValue : 1,
	pkgDoubleValue : 1,
	pkgShortValue : 1,
	pkgLongValue : 1,
	pkgByteValue : 1,
	pkgCharValue : 'X',
	pkgBooleanValue : false,
	stringValue : "I \u4E09\u6708 love \u56DB\u6708 codefinger \u4E94\u6708",
	bigDecimalValue : 1123123123123123.123123123,
	bigIntegerValue : 1123123123123123,
	pkgBooleanArrayValue : [ false, true, false, false ],
	stringArrayValue : [ "\u4E09\u6708", "\r\n\t\f\b\\\/", "codefinger\"\'",
			'Hello' ],
	bigDecimalArrayValue : [ 123.123, 456.45745645, 123123123123 ],
	bigIntegerArrayValue : [ 1, 1111111111111111111111,
			11111111111111111111111, 111 ],
	dateValue : 1438425411860,
	dateArrayValue : [ 1438425412860, 1438425512860, 14384255128607 ],
	gValue : {
		"Hello" : [ [ {
			"123456" : [ {
				"123456.123456" : {
					'1438425412860' : 'A'
				}
			}, {
				"123456.12345789" : {
					'1438425412860' : 'B'
				}
			} ]
		} ] ]
	},
	jsonEnum : "Codefinger",
	jsonEnums : [ "Hello", 1, "MyName", 3, 4, "Please", 6, "Me" ],
}